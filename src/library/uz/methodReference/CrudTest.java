package library.uz.methodReference;

import library.uz.library.Book;
import library.uz.library.Library;
import library.uz.person.Author;
import library.uz.person.Employee;
import library.uz.person.Status;
import library.uz.person.Student;

public class CrudTest {

    public Library createLibrary(Library[] objects, Library object) {

        boolean flag = false;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) continue;
            objects[i] = object;
            flag = true;
            break;
        }

        if (flag)
            return object;
        return null;
    }

    public Employee createEmployee(Employee[] objects, Employee object) {

        boolean flagEmployee = false;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) continue;
            objects[i] = object;
            flagEmployee = true;
            break;
        }

        if (flagEmployee)
            return object;
        return null;
    }

    public Student createStudent(Student[] objects, Student object) {

        boolean flagStudent = false;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) continue;
            objects[i] = object;
            flagStudent = true;
            break;
        }

        if (flagStudent)
            return object;
        return null;
    }

    public Book createBook(Book[] objects, Book object) {

        boolean flagBook = false;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) continue;
            objects[i] = object;
            flagBook = true;
            break;
        }

        if (flagBook)
            return object;
        return null;
    }

    public Author createAuthor(Author[] objects, Author object) {

        boolean flagAuthor = false;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) continue;
            objects[i] = object;
            flagAuthor = true;
            break;
        }

        if (flagAuthor)
            return object;
        return null;
    }

    public Status createStatus(Status[] objects,Status object)
    {
        boolean flagStatus = false;

        for (int i = 0; i < objects.length; i++) {
            if (objects[i] != null) continue;
            objects[i] = object;
            flagStatus = true;
            break;
        }

        if (flagStatus)
            return object;
        return null;
    }

    public boolean returnBookAll(Student student, Status[] objects,int statusId)
    {
        boolean flagStatus = false;

        for (int i = 0; i < objects.length; i++) {
            if(objects[i]!=null && objects[i].getStudentId()==student.getId() && objects[i].getBookId() == statusId)
            {
                objects[i]=null;
                return true;
            }
        }

        return false;
    }

    public boolean returnBookQuantity(Student student, Status[] objects,int statusId)
    {
        boolean flagStatus = false;

        for (int i = 0; i < objects.length; i++) {
            if(objects[i]!=null && objects[i].getStudentId()==student.getId() && objects[i].getBookId() == statusId)
            {
                if(objects[i].getQuantity()==0)
                {
                    objects[i] = null;
                    return true;
                }else{
                    return true;
                }
            }
        }

        return false;
    }

    public int countBooksTake(Student student, Status[] objects)
    {
        int count = 0;
        for (int i = 0; i < objects.length; i++) {
            if(objects[i]!=null)
                continue;
            count++;
        }

        return count;
    }




}
