package library.uz.person;

public class Employee extends Person{

    public Employee(Integer id, String name, String phone, String password, String role, boolean isAdmin) {
        super(id, name, phone, password, role, isAdmin);
    }

    public Employee() {
    }

    public Employee(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
