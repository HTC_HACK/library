package library.uz.person;

import library.uz.library.Book;

public class Status {
    private Integer id;
    private Integer studentId;
    private Integer bookId;
    private String bookName;
    private double quantity;

    public Status() {
    }

    public Status(Integer id, Integer studentId, Integer bookId, String bookName, double quantity) {
        this.id = id;
        this.studentId = studentId;
        this.bookId = bookId;
        this.bookName = bookName;
        this.quantity = quantity;
    }

    public double getQuantity() {
        return quantity;
    }

    public void addQuantity(Status status, double amount) {
        status.setQuantity(status.getQuantity() + amount);
    }

    public void removeBook(Status status, double amount) {
        if (status.getQuantity() >= amount) {
            status.setQuantity(status.getQuantity() - amount);
        }
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStudentId() {
        return studentId;
    }

    public void setStudentId(Integer studentId) {
        this.studentId = studentId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", studentId=" + studentId +
                ", bookId=" + bookId +
                ", bookName='" + bookName + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
