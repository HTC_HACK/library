package library.uz.person;

import library.uz.builder.EmployeeBuilder;
import library.uz.builder.StatusBuilder;
import library.uz.library.Book;
import library.uz.methodReference.CrudTest;
import library.uz.repository.InterfaceHelper;
import library.uz.repository.StudentRepository;

import java.util.Scanner;

public class Student extends Person implements StudentRepository<Book,Student> {

    Status[] studentBooks = new Status[15];
    int statusId = 1;

    public Student() {

    }

    public Student(Integer id, String name, String phone, String password, String role, boolean isAdmin) {
        super(id, name, phone, password, role, isAdmin);
    }

    public Student(String name) {
        super(name);
    }

    //todo::status search =>done
    public Status statusSearch(int optionBook) {
        for (Status status : studentBooks) {
            if (status != null && status.getId() == optionBook) {
                return status;
            }
        }
        return null;
    }


    //todo::take books =>done
    @Override
    public void takeBook(Book book, Student student) {

        book.toString();
        System.out.print("How much : ");
        int quantity = new Scanner(System.in).nextInt();

        if (book.getQuantity() >= quantity) {
            book.removeBook(book, quantity);
            if (book.getQuantity() == 0) {
                book.setAvailable(false);
            }
            for (Status status1 : studentBooks) {
                if (status1 != null && status1.getBookId() == book.getId() && status1.getStudentId() == student.getId()) {
                    status1.addQuantity(status1, quantity);
                    return;
                }
            }
        } else {
            System.out.println(InterfaceHelper.INSUFFICENT_AMOUNT);
            return;
        }

        CrudTest createStudentBook = new CrudTest();
        StatusBuilder statusBuilder = Status::new;
        Status studentBook = statusBuilder.create(statusId, student.getId(), book.getId(), book.getName(), quantity);
        statusId++;
        Status resultStatus = createStudentBook.createStatus(studentBooks, studentBook);


        if (resultStatus != null) {
            System.out.println("You take : " + resultStatus.getBookName() + ", quantity : " + quantity);
            System.out.println("You take again " + createStudentBook.countBooksTake(student, studentBooks) + " type of books");
        } else {
            System.out.println(InterfaceHelper.FAILED);
        }


    }

    //todo::return book all & return quantity =>done
    @Override
    public void returnBook(Book book, Student student) {
        Scanner scannerStr = new Scanner(System.in);
        CrudTest returnBook = new CrudTest();
        System.out.print("How much do you want to return 1=>all, 0=> enter quantity : ");
        int option = scannerStr.nextInt();

        if (option == 1) {
            for (Status status1 : studentBooks) {
                if (status1 != null && status1.getBookId() == book.getId() && status1.getStudentId() == student.getId() && status1.getQuantity() != 0) {
                    book.addQuantity(book, status1.getQuantity());

                    if (book.getQuantity() > 0 && returnBook.returnBookAll(student, studentBooks, book.getId())) {
                        book.setAvailable(true);
                        System.out.println("This " + book.getName() + " all returned successfully");
                        break;
                    } else {
                        System.out.println(InterfaceHelper.FAILED);
                    }

                }
            }
        } else if (option == 0) {
            System.out.print("Enter quantity : ");
            double quantity = scannerStr.nextDouble();
            for (Status status : studentBooks) {
                if (status != null && status.getBookId() == book.getId() && status.getStudentId() == student.getId()) {
                    if (quantity <= status.getQuantity()) {
                        book.addQuantity(book, quantity);
                        if (book.getQuantity() > 0) book.setAvailable(true);
                        status.removeBook(status, quantity);
                        if (returnBook.returnBookQuantity(student, studentBooks, book.getId())) {
                            System.out.println("This " + book.getName() + ", " + quantity + " returned successfully");
                        } else {
                            System.out.println(InterfaceHelper.OOPS);
                        }
                        break;
                    } else {
                        System.out.println(InterfaceHelper.INSUFFICENT_AMOUNT);
                        break;
                    }
                }
            }
        }


    }


    //todo::taken book list =>done;
    @Override
    public void takenBookList(Student student) {
        System.out.println("Books");
        System.out.println("[");
        for (Status status : studentBooks) {
            if (status != null && status.getStudentId() == student.getId()) {
                System.out.println("Book{" +
                        ", bookId=" + status.getBookId() +
                        ", bookName='" + status.getBookName() + '\'' +
                        ", quantity=" + status.getQuantity() +
                        '}');
            }
        }
        System.out.println("]");
    }

}
