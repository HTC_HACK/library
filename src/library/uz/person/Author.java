package library.uz.person;

public class Author extends Person {

    public Author(Integer id, String name, String phone, String password, String role, boolean isAdmin) {
        super(id, name, phone, password, role, isAdmin);
    }

    public Author(Integer id, String name) {
        super(id, name);
    }

    public Author() {

    }

    public Author(String name) {
        super(name);
    }

    public void authorInfo() {
        System.out.println("Name : " + getName());
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + "' }";
    }
}
