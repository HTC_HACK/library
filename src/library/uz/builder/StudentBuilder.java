package library.uz.builder;

import library.uz.person.Status;
import library.uz.person.Student;

@FunctionalInterface
public interface StudentBuilder {

    Student create(Integer id, String name, String phone, String password, String role, boolean isAdmin);
}
