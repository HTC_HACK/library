package library.uz.builder;

import library.uz.person.Author;

@FunctionalInterface
public interface AuthorBuilder {

    Author create(Integer id, String name);
}
