package library.uz.builder;

import library.uz.person.Employee;
import library.uz.person.Person;

@FunctionalInterface
public interface EmployeeBuilder {

    Employee create(Integer id, String name, String phone, String password, String role, boolean isAdmin);

}
