package library.uz.builder;

import library.uz.person.Status;

@FunctionalInterface
public interface StatusBuilder {
    Status create(Integer id, Integer studentId, Integer bookId, String bookName, double quantity);
}
