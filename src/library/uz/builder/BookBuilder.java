package library.uz.builder;

import library.uz.library.Book;
import library.uz.person.Author;

@FunctionalInterface
public interface BookBuilder {
    Book create(Integer id, String category, String name, double quantity, boolean isAvailable, Author author);
}
