package library.uz.builder;

import library.uz.library.Book;
import library.uz.library.Library;
import library.uz.person.Employee;
import library.uz.person.Student;

@FunctionalInterface
public interface LibraryBuilder {

    Library create(Integer id, String name, String address, Book[] books, Employee[] employees, Student[] students);

}
