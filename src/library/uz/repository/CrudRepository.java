package library.uz.repository;

public interface CrudRepository<T,P> {

    public void create(int option);

    public T signIn();

    public P signInEmployee();

    public void signUp();

}