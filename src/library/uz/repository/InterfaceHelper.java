package library.uz.repository;

public interface InterfaceHelper {

    String USER_NOT_FOUND = "User not found";
    String EMPLOYEE_NOT_FOUND = "Employee not found";
    String BOOK_NOT_FOUND = "Book not found";
    String STUDENT_NOT_FOUND = "Student not found";
    String WRONG_OPTION = "Wrong option";
    String TAKEN_BY_SOMEONE = "Book already taken by someone";
    String AUTHOR_NOT_FOUND = "Author not found";
    String NOT_AUTHOR_YET = "No author yet";
    String INSUFFICENT_AMOUNT = "Insufficent amount";
    String CREATED_ACCOUNT = "Account successfully created";
    String PHONE_EXIST = "Phone already exist";
    String CREATED_EMPLOYEE = "Employee Successfully added";
    String BOOK_CREATED = "Book added successfully";
    String CANCEL = "Cancelled";
    String FAILED = "Failed some reason";
    String OOPS = "Success but something wrong";

}