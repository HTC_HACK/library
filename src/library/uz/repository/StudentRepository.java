package library.uz.repository;

public interface StudentRepository<T, P> {

    void takeBook(T book, P student);

    void returnBook(T book, P student);

    void takenBookList(P student);

}