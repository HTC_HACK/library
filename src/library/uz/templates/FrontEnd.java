package library.uz.templates;

import library.uz.Services.LibraryService;
import library.uz.library.Book;
import library.uz.person.Employee;
import library.uz.person.Status;
import library.uz.person.Student;
import library.uz.repository.InterfaceHelper;

import java.util.Scanner;

public class FrontEnd {

    //todo::FrontEnd => done
    public void librarySystem() {
        LibraryService libraryService = new LibraryService();
        Book book = new Book();
        Status status = new Status();

        Scanner scannerInt = new Scanner(System.in);
        boolean active = true;

        while (active) {
            System.out.println("1.Login Library man.\n2.Student Login.\n3.Register as Student.\n4.Exit");
            System.out.print("Enter option : ");
            int optionMenu = scannerInt.nextInt();

            switch (optionMenu) {
                case 1: {
                    Employee employee = libraryService.signInEmployee();
                    if (employee != null) {
                        System.out.println("Successfully login");
                        boolean activeEmployee = true;
                        while (activeEmployee) {
                            System.out.println("1.Add Book.\n2.Add Employee.\n3.Student List.\n4.Book List.\n5.Author List by Book.\n6.Employee list\n7.Logout.");
                            System.out.print("Enter option : ");
                            int employyeOption = scannerInt.nextInt();
                            switch (employyeOption) {
                                case 1: {
                                    libraryService.create(employyeOption);
                                    break;
                                }
                                case 2: {
                                    libraryService.create(employyeOption);
                                    break;
                                }
                                case 3: {
                                    libraryService.studentList();
                                    break;
                                }
                                case 4: {
                                    libraryService.bookList();
                                    break;
                                }
                                case 5: {
                                    libraryService.authorListByBook();
                                    break;
                                }
                                case 6: {
                                    libraryService.employeeList();
                                    break;
                                }
                                case 7: {
                                    activeEmployee = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        System.out.println(InterfaceHelper.EMPLOYEE_NOT_FOUND);
                    }
                    break;
                }
                case 2: {
                    Student student = libraryService.signIn();
                    if (student != null) {
                        boolean studentMenu = true;
                        while (studentMenu) {
                            System.out.println("1.Take Book.\n2.Return Book.\n3.Taken Book List.\n4.Book list.\n5.Logout");
                            int studentOption = scannerInt.nextInt();
                            switch (studentOption) {
                                case 1: {
                                    libraryService.bookList();
                                    System.out.print("Enter book id : ");
                                    int optionBook = scannerInt.nextInt();
                                    Book book1 = libraryService.bookSearch(optionBook);
                                    if (book1 != null && book1.isAvailable()) {
                                        student.takeBook(book1, student);
                                    } else if (book1 != null && !book1.isAvailable()) {
                                        System.out.println(InterfaceHelper.TAKEN_BY_SOMEONE);
                                    } else {
                                        System.out.println(InterfaceHelper.BOOK_NOT_FOUND);
                                    }
                                    break;
                                }
                                case 2: {
                                    student.takenBookList(student);
                                    System.out.print("Enter book id : ");
                                    int optionBook = scannerInt.nextInt();
                                    Book book2 = libraryService.bookSearch(optionBook);
                                    if (book2 != null) {
                                        student.returnBook(book2, student);
                                    } else {
                                        System.out.println(InterfaceHelper.BOOK_NOT_FOUND);
                                    }
                                    break;
                                }
                                case 3: {
                                    student.takenBookList(student);
                                    break;
                                }
                                case 4: {
                                    libraryService.bookList();
                                    break;
                                }
                                case 5: {
                                    studentMenu = false;
                                    break;
                                }
                            }

                        }
                    } else {
                        System.out.println(InterfaceHelper.STUDENT_NOT_FOUND);
                    }
                    break;
                }
                case 3: {
                    libraryService.signUp();
                    break;
                }
                case 4: {
                    active = false;
                    break;
                }
                default: {
                    System.out.println(InterfaceHelper.WRONG_OPTION);
                }
            }

        }
    }

}
