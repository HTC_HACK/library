package library.uz.library;

import library.uz.person.Author;

public class Book {

    private Integer id;
    private String category;
    private String name;
    private double quantity;
    private boolean isAvailable = true;
    private Author author;

    public Book() {
    }

    public Book(Integer id, String category, String name, double quantity, boolean isAvailable, Author author) {
        this.id = id;
        this.category = category;
        this.name = name;
        this.quantity = quantity;
        this.isAvailable = isAvailable;
        this.author = author;
    }


    public String getAuthorName() {
        return author.getName();
    }


    public void setAuthor(Author author) {

        this.author = author;

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public void addQuantity(Book book, double amount) {
        book.setQuantity(book.getQuantity() + amount);
    }

    public void removeBook(Book book, double amount) {
        if (book.getQuantity() >= amount) {
            book.setQuantity(book.getQuantity() - amount);
        }
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", name='" + name + '\'' +
                ", quantity=" + quantity +
                ", isAvailable=" + isAvailable +
                ", author=" + author.toString() +
                '}';
    }
}
