package library.uz.library;

import library.uz.person.Employee;
import library.uz.person.Student;

import java.util.Arrays;

public class Library {

    private Integer id;
    private String name;

    public Library() {
    }

    public Library(Integer id, String name, String address, Book[] books, Employee[] employees, Student[] students) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.books = books;
        this.employees = employees;
        this.students = students;
    }

    private String address;
    private Book[] books;
    private Employee[] employees;
    private Student[] students;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Book[] getBooks() {
        return books;
    }

    public void setBooks(Book[] books) {
        this.books = books;
    }

    public Employee[] getEmployees() {
        return employees;
    }

    public void setEmployees(Employee[] employees) {
        this.employees = employees;
    }

    public Student[] getStudents() {
        return students;
    }

    public void setStudents(Student[] students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return "Library{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", books=" + Arrays.toString(books) +
                ", employees=" + Arrays.toString(employees) +
                ", students=" + Arrays.toString(students) +
                '}';
    }
}
