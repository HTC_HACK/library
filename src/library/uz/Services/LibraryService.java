package library.uz.Services;

import library.uz.builder.*;
import library.uz.library.Book;
import library.uz.library.Library;
import library.uz.methodReference.CrudTest;
import library.uz.person.Author;
import library.uz.person.Employee;
import library.uz.person.Student;
import library.uz.repository.CrudRepository;
import library.uz.repository.InterfaceHelper;

import java.util.Scanner;

public class LibraryService implements CrudRepository<Student, Employee> {

    Library[] libraries = new Library[10];
    int libraryId = 1;

    Student[] students = new Student[100];
    int studentId = 1;

    Employee[] employees = new Employee[15];
    int employeId = 1;

    Book[] books = new Book[150];
    int bookId = 1;

    Author[] authors = new Author[20];
    int authorId = 1;

    //todo::Library create => done
    //todo::Library boss create => done
    public LibraryService() {

        //todo::Library create => done

        CrudTest createLibrary = new CrudTest();
        LibraryBuilder libraryBuilder = Library::new;

        Library library = libraryBuilder.create(libraryId, "Alisher Navoiy",
                "Yunusobod tumani , Navoiy ko'chasi 2A uy", books, employees, students);
        libraryId++;

        Library libraryResult = createLibrary.createLibrary(libraries, library);

        //todo::Library boss create => done
        CrudTest createEmployee = new CrudTest();
        EmployeeBuilder employeeBuilder = Employee::new;

        Employee employee = employeeBuilder.create(employeId, "Admin", "1001010", "1111", "Admin", true);
        employeId++;
        Employee employeeResult = createEmployee.createEmployee(employees, employee);

    }

    //todo::check author by id => done
    public Author checkAuthorExist(int optionId) {
        for (Author author : authors) {
            if (author != null && author.getId() == optionId)
                return author;
        }
        return null;
    }

    //todo::author is exist=>done
    public Author checkAuthor(String authorName) {
        for (Author author : authors) {
            if (author != null && author.getName().equalsIgnoreCase(authorName)) {
                return author;
            }
        }

        return null;
    }

    //todo::setProcessAuthor in author list =>done
    public boolean setProcessAuthor(Author author, String authorName) {
        if (checkAuthor(authorName) != null) {
            return false;
        }
        return true;
    }

    //todo::add book by admin =>done
    //todo::add employee by admin =>done
    @Override
    public void create(int option) {

        Scanner scannerStr = new Scanner(System.in);


        //todo::add book by admin =>done
        if (option == 1) {

            System.out.print("Enter book name : ");
            String name = scannerStr.nextLine();

            System.out.print("Enter book category : ");
            String category = scannerStr.nextLine();

            Author selectedAuthor = null;


            System.out.println("1.Create author. 2.Select author. 3.Cancel");
            int optionCreate = new Scanner(System.in).nextInt();

            if (optionCreate == 1) {
                System.out.print("Enter author : ");
                String authorName = scannerStr.nextLine();
                Author author1 = checkAuthor(authorName);
                if (author1 != null) {
                    selectedAuthor = author1;
                } else {
                    CrudTest authorCreate = new CrudTest();
                    AuthorBuilder authorBuilder = Author::new;

                    Author author = authorBuilder.create(authorId, authorName);
                    authorId++;
                    Author authorResult = authorCreate.createAuthor(authors, author);
                    if (authorResult != null) {
                        selectedAuthor = author;
                    } else {
                        return;
                    }
                }
            } else if (optionCreate == 2) {
                if (isEmpty(authors) == false) {
                    System.out.println(InterfaceHelper.AUTHOR_NOT_FOUND);
                    System.out.println(InterfaceHelper.CANCEL);
                    return;
                }
                authorList();
                System.out.print("Enter author id : ");
                int authorById = new Scanner(System.in).nextInt();

                Author selected = null;

                selected = checkAuthorExist(authorById);
                if (selected != null) {
                    selectedAuthor = selected;
                } else {
                    System.out.println(InterfaceHelper.AUTHOR_NOT_FOUND);
                    return;
                }
            } else {
                System.out.println(InterfaceHelper.CANCEL);
                return;
            }

            System.out.print("How many book do you want this :  ");
            int quantity = new Scanner(System.in).nextInt();

            CrudTest createBook = new CrudTest();
            BookBuilder bookBuilder = Book::new;

            Book book = bookBuilder.create(bookId, category, name, quantity, true, selectedAuthor);
            bookId++;

            Book bookResult = createBook.createBook(books, book);

            if (bookResult != null) {
                System.out.println(bookResult.toString());
                System.out.println(InterfaceHelper.BOOK_CREATED);
            } else {
                System.out.println(InterfaceHelper.FAILED);
            }


        }
        //todo::add employee by admin =>done
        else if (option == 2) {

            System.out.print("Enter employee name : ");
            String name = scannerStr.nextLine();

            System.out.print("Enter phone : ");
            String phone = scannerStr.nextLine();

            System.out.print("Enter password : ");
            String password = scannerStr.nextLine();

            System.out.print("Enter role : ");
            String role = scannerStr.nextLine();

            CrudTest createEmployee = new CrudTest();
            EmployeeBuilder employeeBuilder = Employee::new;

            Employee employee = employeeBuilder.create(employeId, name, phone, password, role, true);
            employeId++;
            Employee employeeResult = createEmployee.createEmployee(employees, employee);

            if (employeeResult != null) {
                System.out.println(employeeResult.toString());
                System.out.println(InterfaceHelper.CREATED_EMPLOYEE);
            } else {
                System.out.println(InterfaceHelper.FAILED);
            }
        }

    }

    //todo::student sigin =>done
    @Override
    public Student signIn() {

        Scanner scannerStr = new Scanner(System.in);

        Student student = new Student();

        System.out.print("Enter phone : ");
        String phone = scannerStr.nextLine();
        student.setPhone(phone);

        System.out.print("Enter password : ");
        String password = scannerStr.nextLine();
        student.setPassword(password);

        for (Student student1 : students) {
            if (student1 != null && student.getPhone().equals(student1.getPhone()) && student.getPassword().equals(student1.getPassword())) {
                return student1;
            }
        }

        return null;
    }

    //todo::employee sigin => done
    @Override
    public Employee signInEmployee() {
        Scanner scannerStr = new Scanner(System.in);

        Employee employee = new Employee();

        System.out.print("Enter phone : ");
        String phone = scannerStr.nextLine();
        employee.setPhone(phone);

        System.out.print("Enter password : ");
        String password = scannerStr.nextLine();
        employee.setPassword(password);

        for (Employee employee1 : employees) {
            if (employee1 != null && employee.getPhone().equals(employee1.getPhone()) && employee.getPassword().equals(employee1.getPassword())) {
                return employee1;
            }
        }

        return null;
    }

    //todo::register student sigup => done
    @Override
    public void signUp() {

        Scanner scannerStr = new Scanner(System.in);

        System.out.print("Enter name : ");
        String name = scannerStr.nextLine();

        System.out.print("Enter phone : ");
        String phone = scannerStr.nextLine();

        if (checkPhone(phone)) {

            System.out.print("Enter password : ");
            String password = scannerStr.nextLine();

            CrudTest createStudent = new CrudTest();
            StudentBuilder studentBuilder = Student::new;

            Student student = studentBuilder.create(studentId, name, phone, password, "Student", false);
            studentId++;
            Student resultStudent = createStudent.createStudent(students, student);

            if (resultStudent != null) {
                System.out.println(InterfaceHelper.CREATED_ACCOUNT);
            } else {
                System.out.println(InterfaceHelper.FAILED);
            }

        } else {
            System.out.println(InterfaceHelper.PHONE_EXIST);
        }


    }

    //todo::bookList  => done
    public void bookList() {
        for (Book book : books) {
            if (book != null && book.isAvailable()) {
                System.out.println("id : " + book.getId() + ", name : " + book.getName() + ", author : " + book.getAuthorName() + ", category : " + book.getCategory() + ", quantity : " + book.getQuantity());
            } else if (book != null && !book.isAvailable()) {
                System.out.println("Not available = id : " + book.getId() + ", name : " + book.getName() + ", author : " + book.getAuthorName() + ", category : " + book.getCategory() + ", quantity : " + book.getQuantity());
            }
        }
    }

    //todo::employeeList  => done
    public void employeeList() {
        for (Employee employee : employees) {
            if (employee != null)
                System.out.println("id : " + employee.getId() + ", name : " + employee.getName() + ", role : " + employee.getRole() + ", phone : " + employee.getPhone());
        }
    }

    //todo::studentList with books => done
    public void studentList() {
        System.out.println("\n");
        for (Student student : students) {
            if (student != null) {
                System.out.println("Student info");
                System.out.println("[ id : " + student.getId() + ", name : " + student.getName() + ", phone : " + student.getPhone() + " ]");
                student.takenBookList(student);
                System.out.println("\n");
            }
        }
    }

    //todo::authorList with books  => done
    public void authorListByBook() {
        int flag = 0;
        Scanner scannerStr = new Scanner(System.in);
        System.out.print("Enter author name : ");
        String name = scannerStr.nextLine();

        for (Book book1 : books) {
            if (book1 != null && book1.getAuthorName().equalsIgnoreCase(name)) {
                System.out.println("id : " + book1.getId() + ", name : " + book1.getName() + ", category : " + book1.getCategory());
                flag++;
            }
        }

        if (flag == 0) {
            System.out.println(InterfaceHelper.AUTHOR_NOT_FOUND);
        }

    }

    //todo::checkPhone isAlreadyExist => done
    public boolean checkPhone(String phone) {
        for (Student student : students) {
            if (student != null && student.getPhone().equals(phone)) {
                return false;
            }
        }
        return true;
    }

    //todo::this book is Exist by id => done
    public Book bookSearch(int booksId) {
        for (Book book : books) {
            if (book != null && book.getId() == booksId) {
                return book;
            }
        }
        return null;
    }


    //todo::authorList createing book => done
    public void authorList() {
        int flag = 0;

        for (Author author : authors) {
            if (author != null) {
                System.out.println("Person{" +
                        "id=" + author.getId() +
                        ", name='" + author.getName() + "' }");
                flag++;
            }
        }
        if (flag == 0) {
            System.out.println(InterfaceHelper.NOT_AUTHOR_YET);
        }
    }

    //todo::no author in author list=>done
    public boolean isEmpty(Author[] authors) {

        for (Author author : authors) {
            if (author != null)
                return true;
        }

        return false;
    }


}